// functions.js

console.log('- functions.js loaded'.yellow);

var crypto = require('crypto');
var db = require('./db');
var dataAccess = require('./dataAccess');
var moment = require('moment');

module.exports = {

    /** Middleware for limited access and admin interface */
    requireLogin: function (req, res, next) {
        sess = req.session;
        if (sess.id_hospital) {
            next();
        } else {
            res.redirect("/");
        }
    },

    /** Middleware check session exist */
    notLogged: function (req, res, next) {
        var sess = req.body;
        if (sess.login != null) {
            res.redirect('/home');
        } else {
            next();
        }
    },
    
    checkPass: function (hospital_list, name, sess, callback) {
        for (let i in hospital_list) {
            if (name == hospital_list[i].name_hospital) {
                console.log(name.blue + ' is connected');
                sess.id_hospital = hospital_list[i].id_hospital;
                sess.name_hospital = hospital_list[i].name_hospital;
                sess.image_user = 'main.png';
                sess.login = 'Be or not to be';
                callback(1);
            }
        }
    },

    checkBacteria: function(bodyOfOutput, b_list, callback) {
        let bacteria_list_add = [], bacteria_list = [];
        for (let i in bodyOfOutput) {
            for (let v in bodyOfOutput[i].bacts) {
                b_list.some(b => b.name_bacteria == bodyOfOutput[i].bacts[v].name) ? bacteria_list.push(bodyOfOutput[i].bacts[v].name) : bacteria_list_add.push(bodyOfOutput[i].bacts[v].name);
            }
        }
        callback(bacteria_list_add, bacteria_list);
    },

    checkLinksBacterias: function(b_list, bacteria_list, bacteria_list_add, callback) {
        let list_to_add_links = [];

        for (let i in b_list) {
            for (let v in bacteria_list) 
                bacteria_list[v].name_bacteria == b_list[i].name_bacteria ? list_to_add_links.push(bacteria_list[v]) : {};
            for (let u in bacteria_list_add)
                bacteria_list_add[u].name_bacteria == b_list[i].name_bacteria ? list_to_add_links.push(bacteria_list_add[u]) : {};
        }
        callback(list_to_add_links);
    }
};
