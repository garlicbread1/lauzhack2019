/* jshint esversion: 6 */
const {Docker} = require('node-docker-api');
const fs = require('fs');
const path = require('path');

const docker = new Docker({socketPath : '/var/run/docker.sock'});

const folderName = './roomResults';
function mkdirRoomFolder() {
  let roomPath = path.join(__dirname, folderName);
  if(!fs.existsSync(roomPath)) {
    fs.mkdirSync(roomPath, {recursive: true});
  }
  return roomPath;
}

const promisifyStream = stream => new Promise((resolve, reject) => {
  stream.on('data', data => console.log(data.toString()));
  stream.on('end', resolve);
  stream.on('error', reject);
});

/**
 * Returns a promise to notice when a container is done processing genetic data
 */
const containerSrcOutputPath = '/src/output';
exports.getBacteriasFromRoom = (roomName, datasetUrl) => {
  let roomPath = mkdirRoomFolder();
  let docker_img1 = {
    Image: 'vonderwe/metaphlan:latest',
    name: 'metaphlan_' + roomName.toString(),
    HostConfig: {
      Binds: [
        roomPath.toString() + ":" + containerSrcOutputPath
      ]
    },
    Cmd: [datasetUrl, roomName.toString()],
  };
  return docker.container.create(docker_img1)
  
  .then(container => container.start())
  .catch(err => console.log("Could not create instance for metaphlan : " + err))
  .then(container => container.delete({ force: true }));
};

/**
 * Returns a promise to notice when a container is done processing results data
 */
exports.analyzeBacteriaFromRoom = () => {
  return docker.container.create({
    // TODO: fill name
    Image: 'vonderwe/genetic-room-correlation:latest',
    name: 'genetic-room-correlation-processing',
    HostConfig: {
      Binds: [
        mkdirRoomFolder() + ":" + containerSrcOutputPath
      ]
    },
  })
  .then(container => container.start())
  .then(container => container.delete({ force: true }));
};

exports.readFinalResult = () => {
  let file_content = fs.readFileSync(path.join(mkdirRoomFolder(), 'final_result.json'))
  return JSON.parse(file_content);
};

//exports.getBacteriasFromRoom("test", "https://lauzhack2019.blob.core.windows.net/data/dataset_2/0195240287_1.fastq.gz?sv=2019-02-02&ss=b&srt=sco&sp=rwdlac&se=2019-11-18T07:45:33Z&st=2019-11-15T23:45:33Z&spr=https&sig=tgeZyvviE6hxD%2F53ZYsAYpJPcdozHWjyC6q%2FSImm0NQ%3D");