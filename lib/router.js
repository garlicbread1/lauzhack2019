/* List of the path of the application */

console.log('- router.js loaded'.yellow);

let functions = require('./function');
let dataAccess = require('./dataAccess');
let docker_client = require('./docker_launcher');


module.exports = function (app, socket) {
    app.get('/', function (req, res, next) {
        let erreur = false;
        if (req.query.failed)
        erreur = true;
        res.render('../view/login.ejs', {
            title: 'LauzHack - Garlic Beard',
            erreur: erreur
        });
    });
    
    app.post('/login', function (req, res) {
        let sess = req.session;
        dataAccess.findAll('lauz_hospital', function (hospital_list) {
            functions.checkPass(hospital_list, req.body.login, sess, function(logged) {
                if (logged) {
                    res.redirect('/home');
                } else console.log('smthg not good append');
            });
        });
    });
    
    app.get('/home', [functions.requireLogin], function (req, res) {
        dataAccess.findAll('lauz_hospital', function (hospital_list) {
            dataAccess.findAll('lauz_room', function (rooms_list) {
                res.render("../view/home.ejs", {
                    title: "LauzHack - Garlic Beard",
                    hospital_list: hospital_list,
                    rooms_list: rooms_list,
                    sess: sess
                });
            });
        });
    });
    
    app.get('/rooms', [functions.requireLogin], function (req, res) {
        dataAccess.findAll('lauz_hospital', function (hospital_list) {
            dataAccess.findAll('lauz_room', function (rooms_list) {
                dataAccess.findAll('lauz_links', function (links_list) {
                    res.render("../view/rooms.ejs", {
                        title: "LauzHack - Garlic Beard",
                        hospital_list: hospital_list,
                        rooms_list: rooms_list,
                        links_list: links_list,
                        sess: sess
                    });
                });
            });
        });
    });
    
    app.get('/results', [functions.requireLogin], function (req, res) {
        dataAccess.findAll('lauz_hospital', function (hospital_list) {
            dataAccess.findAll('lauz_room', function (rooms_list) {
                dataAccess.findAll('lauz_analyse', function (analyses_list) {
                    dataAccess.findAll('lauz_results', function (results_list) {
                        res.render("../view/results.ejs", {
                            title: "LauzHack - Garlic Beard",
                            hospital_list: hospital_list,
                            rooms_list: rooms_list,
                            analyses_list: analyses_list,
                            results_list: results_list,
                            sess: sess
                        });
                    });
                });
            });
        });
    });
    
    app.get('/analyses', [functions.requireLogin], function (req, res) {
        dataAccess.findAll('lauz_analyse', function (analyses_list) {
            res.render("../view/analyses.ejs", {
                title: "LauzHack - Garlic Beard",
                analyses_list: analyses_list,
                sess: sess
            });
        });
    });
    
    app.get('/add_hospital', [functions.requireLogin], function (req, res) {
        res.render("../view/add_hospital.ejs", {
            title: "LauzHack - Garlic Beard",
            sess: sess
        });
    });
    
    app.post('/add_hospital', [functions.requireLogin], function (req, res) {
        dataAccess.addHospital(req.body.name_hospital, function(v_res) {
            v_res ? res.redirect('/home') : {};
        });
    });
    
    app.get('/add_room', [functions.requireLogin], function (req, res) {
        dataAccess.findAll('lauz_hospital', function(hospital_list) {
            res.render("../view/add_room.ejs", {
                title: "LauzHack - Garlic Beard",
                hospital_list: hospital_list,
                sess: sess
            });
        });
    });
    
    app.post('/add_room', [functions.requireLogin], function (req, res) {
        dataAccess.addRoom(parseInt(req.body.name_hospital), req.body.name_room, function (v_res) {
            v_res ? res.redirect('/rooms') : {};
        });
    });
    
    app.get('/add_analyse', [functions.requireLogin], function (req, res) {
        dataAccess.findAll('lauz_room', function (rooms_list) {
            dataAccess.findAll('lauz_hospital', function (hospital_list) {
                res.render("../view/add_analyse.ejs", {
                    title: "LauzHack - Garlic Beard",
                    rooms_list: rooms_list,
                    hospital_list: hospital_list,
                    sess: sess
                });
            });
        });
    });
    
    app.post('/add_analyse', [functions.requireLogin], function (req, res) {
        let total_vres = 0;
        let last;
        
        dataAccess.addUniqueAnalyse(function(res_) { });
        
        dataAccess.findAll('lauz_analyse', function(list_analyses) {
            last = list_analyses.reduce( (acc, a) => {
                if (acc == undefined || a.date > acc.date) return a;
                else return acc;
            }, undefined);
            for (let i in req.body.name_room) {
                dataAccess.addAnalyse(last.id_analyse, parseInt(req.body.name_room[i]), req.body.value_url[i], function (v_res) {
                    v_res ? total_vres++ : {};
                    total_vres == req.body.name_room.length ? console.log('ok') : {};
                });
            }
            
            let dockers = []
            for (let i in req.body.name_room) {
                dockers.push(docker_client.getBacteriasFromRoom(parseInt(req.body.name_room[i]), req.body.value_url[i]).then(() => { this.promise.done = true; }));
            }
            
            new Promise((resolve, reject) => 
            {   
                setTimeout(() => {
                    while (!dockers.every(d => d.done === true)) {}
                    resolve();
                }, 100);
            }).then(() => {
                docker_client.analyzeBacteriaFromRoom().then(() => {
                    let result = docker_client.readFinalResult();
                    let cpt_v = 0;
                    dataAccess.findAll('lauz_bacteria', function (b_list) {
                        functions.checkBacteria(result, b_list, function(bacteria_list_add, bacteria_list) {
                            console.log('Before inserting new bacterias in the DB: ' + bacteria_list_add);
                            console.log('Before inserting known bacterias in the DB: ' + bacteria_list);
                            for (let i in bacteria_list_add) {
                                dataAccess.addBacterias(bacteria_list_add[i], function (res_v){
                                    res_v ? cpt_v++ : {};
                                    cpt_v == bacteria_list_add.length ? console.log('Evthg. new added') : {};
                                });
                            }
                            dataAccess.findAll('lauz_bacteria', function (b_list_v) {
                                let cfpt = 0;
                                for (let v in result) {
                                    dataAccess.addResult(result[v].name, last.id_analyse, (res_hhh) => {
                                        dataAccess.findAll('lauz_results', function(list_results) {
                                            last_r = list_results.reduce( (acc, a) => {
                                                if (acc == undefined || a.date > acc.date) return a;
                                                else return acc;
                                            }, undefined);
                                            for (let k in result[v].bacts) {
                                                dataAccess.addLinksBacterias(b_list_v.filter(b => b.name_bacteria == result[v].bacts[k].name).map(b => b.id_bacteria)[0], result[v].bacts[k].percent, last_r.id_result, function(s) {
                                                    s ? cfpt++ : {};
                                                    cfpt == list_to_add_links.length ? console.log("Done with data processing") : {};
                                                });
                                            }
                                        });
                                    });
                                }
                                dataAccess.updateStatus(last.id_analyse, (res_rr) => {
                                    console.log("Status updated");
                                });
                            });
                        });
                    });
                });
            });
            res.redirect('/analyses');
        });
    });
    
    app.post('/search', [functions.requireLogin], function(req, res) {
        dataAccess.findAll('lauz_hospital', function (hospital_list) { 
            dataAccess.findAll('lauz_links', function (links_list) { 
                dataAccess.searchRoom(req.body.search_v, function (rooms_list) {
                    res.render('../view/rooms.ejs', {
                        title: "LauzHack - Garlic Beard",
                        rooms_list: rooms_list,
                        hospital_list: hospital_list,
                        links_list: links_list,
                        sess: sess
                    });
                });
            });
        });
    });
    
    app.get('/hospital:id', [functions.requireLogin], function(req, res) {
        dataAccess.findAll('lauz_room', function(rooms_list) {
            dataAccess.findAll('lauz_links', function(links_list) {
                dataAccess.findElementsById(req.params.id, 'lauz_hospital', 'id_hospital', function(hospital_list){
                    dataAccess.findAll('lauz_results', function(results_list) {
                        res.render('../view/hospital_graph.ejs', {
                            title: "LauzHack - Garlic Beard",
                            rooms_list: rooms_list,
                            hospital_list: hospital_list,
                            links_list: links_list,
                            results_list: results_list,
                            id: req.params.id,
                            sess: sess
                        });
                    });
                });
            });
        });
    });
    
    app.get('/result:id', [functions.requireLogin], function(req, res) {
        dataAccess.findElementsById(req.params.id, 'lauz_results', 'id_analyse', function(results_list) {
            dataAccess.findAll('lauz_hospital', function(hospital_list) {
                dataAccess.findAll('lauz_link_bacteria', function(links_bacteria_list) {
                    dataAccess.findAll('lauz_bacteria', function(bacteria_list) {
                        res.render("../view/fiche_result.ejs", {
                            title: "LauzHack - Garlic Beard",
                            results_list: results_list,
                            hospital_list: hospital_list,
                            links_bacteria_list: links_bacteria_list,
                            bacteria_list: bacteria_list,
                            id: req.params.id,
                            sess: sess
                        });
                    });
                });
            });
        });
    });
    
    app.get('/add_links', [functions.requireLogin], function(req, res) {
        dataAccess.findAll('lauz_hospital', function(hospital_list) {
            dataAccess.findAll('lauz_room', function(rooms_list) {
                res.render("../view/link_rooms.ejs", {
                    title: "LauzHack - Garlic Beard",
                    hospital_list: hospital_list,
                    rooms_list: rooms_list,
                    sess: sess
                });
            });
        });
    });
    
    app.post('/add_links', [functions.requireLogin], function(req, res) {
        dataAccess.addLinks(req.body.first_room, req.body.second_room, function(v_res) {
            v_res ? res.redirect('/rooms') : {}; 
        });
    });
    
    // TODO : Route for checking and adding list of bacterias to the DB from the output file
    app.post('/bacteria', [functions.requireLogin], function(req, res) {
        let cpt_v = 0;
        console.log('Before parsing in the function: ' + req.body);
        dataAccess.findAll('lauz_bacteria', function (b_list) {
            functions.checkBacteria(req.body, b_list, function(bacteria_list_add, bacteria_list) {
                console.log('Before inserting new bacterias in the DB: ' + bacteria_list_add);
                console.log('Before inserting known bacterias in the DB: ' + bacteria_list);
                for (let i in bacteria_list_add) {
                    dataAccess.addBacterias(bacteria_list_add[i], function (res_v){
                        res_v ? cpt_v++ : {};
                        cpt_v == bacteria_list_add.length ? console.log('Evthg. new added') : {};
                    });
                }
                dataAccess.findAll('lauz_bacteria', function (b_list_v) {
                    let cfpt = 0;
                    functions.checkLinksBacterias(b_list_v, bacteria_list, bacteria_list_add, function(list_to_add_links) {
                        for (let v in list_to_add_links) {
                            dataAccess.addLinksBacterias(list_to_add_links[v], req.body.id_result, function(s){
                                s ? cfpt++ : {};
                                cfpt == list_to_add_links.length ? console.log('send socket because its ok') : {};
                            });
                        }
                    });
                });
            });
        });
    });
    
    // TODO : Emit a socket when status is update
    app.post('/update_status:id_analyse', [functions.requireLogin], function(req, res) {
        dataAccess.updateStatus(req.params.id_analyse, function(resv) {
            resv ? console.log('emit socket status update on analyse') : {};
        });
    });
    
    // TODO : GET if it's easier ?
    app.get('/update_status:id_analyse', [functions.requireLogin], function (req, res) {
        dataAccess.updateStatus(req.params.id_analyse, function (resv) {
            resv ? console.log('emit socket status update on analyse') : {};
        });
    });
    
    app.get('/bacteria', [functions.requireLogin], function(req, res) {
        dataAccess.findAll('lauz_bacteria', function(bacteria_list) {
            dataAccess.findAll('lauz_link_bacteria', function (bacteria_links_list) {
                res.render('../view/bacterias.ejs', {
                    title: "LauzHack - Garlic Beard",
                    bacteria_links_list: bacteria_links_list,
                    bacteria_list: bacteria_list,
                    sess: sess
                });
            });
        });
    });
    
    app.get('/help', [functions.requireLogin], function(req, res) {
        res.render('../view/help.ejs', {
            title: "LauzHack - Garlic Beard",
            sess: sess
        });
    });
    
    app.get('/logout', function (req, res) {
        req.session.destroy(function () {
            res.redirect("/");
        });
    });
};