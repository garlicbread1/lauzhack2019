/* Functions of requests on the database */

console.log('- dataAccess.js loaded'.yellow);

var mysql = require('mysql');
var db = require('./db');
var functions = require('./function');

/**
Module who will permit to create the functions for the requests on the DataBase
**/
module.exports = {

        findAll: function (table, callback) {
            var list = [];
            var queryString = 'SELECT * FROM ' + table;
            db.query(queryString, function (err, rows) {
                if (err)
                    throw err;
                for (var i in rows)
                    list.push(rows[i]);
                callback(list);
            });
        }, 


        findElementsById: function (id, table, column, callback) {
            var list = [];
            var queryString = 'SELECT * FROM ' + table + ' where ' + column + '=' + id + ';';
            db.query(queryString, function (err, rows) {
                if (err)
                    throw err;
                for (var i in rows)
                    list.push(rows[i]);
                callback(list);
            });
        },

        addHospital: function(name, callback) {
            var queryString = 'INSERT INTO lauz_hospital (name_hospital) VALUES (?);';
            db.query(queryString, [name], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        addRoom: function (id_hospital, name_room, callback) {
            var queryString = 'INSERT INTO lauz_room (id_hospital, name_room) VALUES (?,?);';
            db.query(queryString, [id_hospital, name_room], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        addAnalyse: function (id_hospital, id_room, value_url, callback) {
            var queryString = 'INSERT INTO lauz_room_analyse (id_room, id_analyse, url) VALUES (?,?,?);';
            db.query(queryString, [id_room, id_hospital, value_url], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        addUniqueAnalyse: function (callback) {
            var queryString = 'INSERT INTO lauz_analyse () VALUES ();';
            db.query(queryString, function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        searchRoom: function(v_search, callback) {
            var list = [];
            var queryString = 'SELECT * FROM lauz_room where name_room like "%' + v_search + '%";';
            db.query(queryString, function (err, rows) {
                if (err)
                    throw err;
                for (var i in rows)
                    list.push(rows[i]);
                callback(list);
            });
        },

        addLinks: function(first_room, second_room, callback) {
            var queryString = 'INSERT INTO lauz_links (id_first_room, id_second_room) VALUES (?,?);';
            db.query(queryString, [first_room, second_room], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        addBacterias: function(bacteria, callback) {
            var queryString = 'INSERT INTO lauz_bacteria (name_bacteria) VALUES (?);';
            db.query(queryString, [bacteria.name_bacteria], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        addLinksBacterias: function(bacteria, percent, id_result, callback) {
            var queryString = 'INSERT INTO lauz_link_bacteria (id_result, id_bacteria, percent) VALUES (?,?,?);';
            db.query(queryString, [id_result, bacteria, percent], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        updateStatus: function(id_analyse, callback) {
            var queryString = 'UPDATE lauz_analyse SET status=1 where id_analyse=?;';
            db.query(queryString, [id_analyse], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        },

        addResult: function(id_room, id_analyse, callback) {
            var queryString = 'INSERT INTO lauz_results (id_room, id_analyse) VALUES (?,?);';
            db.query(queryString, [id_room, id_analyse], function (err, rows) {
                if (err) {
                    throw err;
                    callback(0);
                } else callback(1);
            });
        }
};