#!/bin/sh

filename="data.fastq.gz"

if [ $1 = "help" ]
then
	echo "Usage: docker run --mount type=bind,src=[work_dir]/,dst=/src/output metaphlan [link_to_fastq.gz_file] [room_name]"
	exit 0
fi
if [ "$#" -ne 2 ]
then
	echo "Usage: docker run --mount type=bind,src=[work_dir]/,dst=/src/output metaphlan [link_to_fastq.gz_file] [room_name]"
	exit 1
fi

if [ -e $filename ]
then
	rm $filename
fi

if [ ! -d "./output" ]
then
	mkdir "./output"
fi

echo "Downloading $1"
wget "$1" -O $filename


echo "Calculating room profile"
metaphlan2.py --input_type fastq $filename > "./output/bacteria_profile_$2.txt"

rm *.bowtie2out.txt

