#! /usr/bin/env python3

import re
import os
import json
import sys


class Node:
    def __init__(self, name: str, prob):
        self.name = name
        self.children = []
        self.prob = prob

    def getNodeFromValue(self, name: str):
        if self.name == name:
            return self
        if len(self.children) == 0:
            return None
        for i in range(len(self.children)):
            node = self.children[i].getNodeFromValue(name)
            if node:
                return node
        return None

    def __str__(self):
        return "{}: {} %".format(self.name, self.prob * 100)

    def print_tree(self, level=0):
        print('\t' * level + str(self))
        for child in self.children:
            child.print_tree(level+1)

    def get_leaf_nodes(self):
        leafs = []

        def _get_leaf_nodes(node):
            if node is not None:
                if len(node.children) == 0:
                    leafs.append(node)
                for n in node.children:
                    _get_leaf_nodes(n)

        _get_leaf_nodes(self)
        return leafs
    
basepath = sys.argv[1]
current_node_name: str
parent_node_name: str
current_node_probability: float
room_bacteria_dict = dict()
bacterias_list = []
best_node_bacteria = 0.0

for filename in os.listdir(basepath):
    file = open(basepath + '/' + filename, 'r')
    file_stream = file.readlines()
    file.close()
    root = None
    for i, line in enumerate(file_stream):
        # Parsing string to list
        current_line_array = re.split('[\t\n|]', line)
        # remove empty column
        current_line_array = list(filter(None, current_line_array))
        # Don't add "#SampleID	Metaphlan2_Analysis"
        if current_line_array[0] == '#SampleID':
            continue
        current_node_name = current_line_array[-2]
        current_node_probability = float(current_line_array[-1])/100.0
        # root node
        if i == 1:
            root = Node(current_node_name, current_node_probability)
            continue
        parent_node_name = current_line_array[-3]
        # Get parent
        parent_node = root.getNodeFromValue(parent_node_name)
        # Create the node
        current_node = Node(current_node_name, current_node_probability)
        # Add current node to his parent
        parent_node.children.append(current_node)
    leaf_node_list = root.get_leaf_nodes()
    leaf_mean = 0.0
    for leaf in leaf_node_list:
        leaf_mean += leaf.prob
    room_bacteria_dict.update({filename: leaf_node_list})
    #root.print_tree()

output_dict = []
for room, bacterias_room in room_bacteria_dict.items():
    bacts = []
    for bacteria in bacterias_room:
        bacterias_list.append(bacteria.name)
        bacts.append({"name": bacteria.name, "percent": bacteria.prob})
    name = room.split("_")[2].split(".")[0]
    output_dict.append({"name": name, "bacts": bacts})
list(set(bacterias_list))

with open("{}/final_result.json".format(sys.argv[1]), "w") as result:
    result.write(json.JSONEncoder().encode(output_dict))

for bacteria in bacterias_list:
    for room, bacterias_room in room_bacteria_dict.items():
        tmp_best_proba = 0.0
        for bacteria_item in bacterias_room:
            if bacteria_item.prob > tmp_best_proba:
                best_node_bacteria = bacteria_item
            tmp_best_proba = bacteria_item.prob
