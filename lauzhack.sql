-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 16, 2019 at 11:12 PM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lauzhack`
--
CREATE DATABASE IF NOT EXISTS `lauzhack` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lauzhack`;

-- --------------------------------------------------------

--
-- Table structure for table `lauz_analyse`
--

CREATE TABLE `lauz_analyse` (
  `id_analyse` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lauz_hospital`
--

CREATE TABLE `lauz_hospital` (
  `id_hospital` int(11) NOT NULL,
  `name_hospital` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lauz_links`
--

CREATE TABLE `lauz_links` (
  `id_link` int(11) NOT NULL,
  `id_first_room` int(11) NOT NULL,
  `id_second_room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lauz_results`
--

CREATE TABLE `lauz_results` (
  `id_result` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `id_analyse` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accuracy_pwnd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lauz_room`
--

CREATE TABLE `lauz_room` (
  `id_room` int(11) NOT NULL,
  `id_hospital` int(11) NOT NULL,
  `name_room` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lauz_room_analyse`
--

CREATE TABLE `lauz_room_analyse` (
  `id_room_analyse` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `id_analyse` int(11) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lauz_analyse`
--
ALTER TABLE `lauz_analyse`
  ADD PRIMARY KEY (`id_analyse`);

--
-- Indexes for table `lauz_hospital`
--
ALTER TABLE `lauz_hospital`
  ADD PRIMARY KEY (`id_hospital`);

--
-- Indexes for table `lauz_links`
--
ALTER TABLE `lauz_links`
  ADD PRIMARY KEY (`id_link`),
  ADD KEY `fk_first_room_link` (`id_first_room`),
  ADD KEY `fk_second_room_link` (`id_second_room`);

--
-- Indexes for table `lauz_results`
--
ALTER TABLE `lauz_results`
  ADD PRIMARY KEY (`id_result`),
  ADD KEY `fk_results_room` (`id_room`),
  ADD KEY `fk_results_analyse` (`id_analyse`);

--
-- Indexes for table `lauz_room`
--
ALTER TABLE `lauz_room`
  ADD PRIMARY KEY (`id_room`),
  ADD KEY `fk_room_hospital` (`id_hospital`);

--
-- Indexes for table `lauz_room_analyse`
--
ALTER TABLE `lauz_room_analyse`
  ADD PRIMARY KEY (`id_room_analyse`),
  ADD KEY `fk_room_analyse_room` (`id_room`),
  ADD KEY `fk_analyse_analyse_room` (`id_analyse`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lauz_analyse`
--
ALTER TABLE `lauz_analyse`
  MODIFY `id_analyse` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lauz_hospital`
--
ALTER TABLE `lauz_hospital`
  MODIFY `id_hospital` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lauz_links`
--
ALTER TABLE `lauz_links`
  MODIFY `id_link` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lauz_results`
--
ALTER TABLE `lauz_results`
  MODIFY `id_result` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lauz_room`
--
ALTER TABLE `lauz_room`
  MODIFY `id_room` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lauz_room_analyse`
--
ALTER TABLE `lauz_room_analyse`
  MODIFY `id_room_analyse` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lauz_links`
--
ALTER TABLE `lauz_links`
  ADD CONSTRAINT `fk_first_room_link` FOREIGN KEY (`id_first_room`) REFERENCES `lauz_room` (`id_room`),
  ADD CONSTRAINT `fk_second_room_link` FOREIGN KEY (`id_second_room`) REFERENCES `lauz_room` (`id_room`);

--
-- Constraints for table `lauz_results`
--
ALTER TABLE `lauz_results`
  ADD CONSTRAINT `fk_results_analyse` FOREIGN KEY (`id_analyse`) REFERENCES `lauz_analyse` (`id_analyse`),
  ADD CONSTRAINT `fk_results_room` FOREIGN KEY (`id_room`) REFERENCES `lauz_room` (`id_room`);

--
-- Constraints for table `lauz_room`
--
ALTER TABLE `lauz_room`
  ADD CONSTRAINT `fk_room_hospital` FOREIGN KEY (`id_hospital`) REFERENCES `lauz_hospital` (`id_hospital`);

--
-- Constraints for table `lauz_room_analyse`
--
ALTER TABLE `lauz_room_analyse`
  ADD CONSTRAINT `fk_analyse_analyse_room` FOREIGN KEY (`id_analyse`) REFERENCES `lauz_analyse` (`id_analyse`),
  ADD CONSTRAINT `fk_room_analyse_room` FOREIGN KEY (`id_room`) REFERENCES `lauz_room` (`id_room`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
