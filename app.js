global.PORT = 9999;

/* Importation modules */
let express = require('express');
let app = express();
let http = require('http').Server(app);
let bodyParser = require('body-parser');
let session = require('express-session');
let fs = require('fs');
let helmet = require('helmet');
let colors = require('colors');

/* Supports encoded bodies and json encoded bodies */
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + "/public"));
app.set('view engine', 'ejs');


app.use(session({
    secret: 'fd7AF9Gg9c1sdf9.#Zd8a/4J*',
    name: 'sesSionId478',
    cookie: {
        secure: false,
        httpOnly: true
    },
    resave: false,
    saveUninitialized: true,
}));

/* Importation des modules externes */
console.log("\nLoading custom modules:".blue);
let db = require('./lib/db');
let dataAccess = require('./lib/dataAccess');
let socket = require('./lib/socket')(http);
let routes = require('./lib/router')(app, socket);
let functions = require('./lib/function');

/* Server informations */
http.listen(global.PORT, function () {
    console.log(('\n' + "=== App Started on PORT " + global.PORT + '\n').green);
});
