function go_home() {
    document.location.href = "/home";
}

function go_rooms() {
    document.location.href = "/rooms";
}

function go_results() {
    document.location.href = "/results";
}

function go_analyses() {
    document.location.href = "/analyses";
}

function logout() {
    document.location.href = "/logout";
}

function go_help() {
    document.location.href = '/help';
}

function go_bacteria() {
    document.location.href = '/bacteria';
}

var socket = io.connect('http://localhost:9999');

socket.on('res_output', function (data) {
    console.log('Data received: ' + data);
});