# Genomic Garlic Bread 

Genomic garlic bread is a processing pipeline build during the lauzhack2019 for hospitals which would help about potentials threats of sanitary in the rooms of the hospital in question.

## Challenge

The Challenges was proposed by SOPHIA Genetics which was about comparing metagenomics to detect potential threats in hospitals.

## Prerequisite

- nodejs
- python3
- docker
- mysql

## Try to run it

First import the `lauzhack.sql` into your database

Then adjust setting in `lib/db.js`

finally simply run `npm install && node app.js`

and then go to [http://127.0.0.1:9999/](http://127.0.0.1:9999)


## Authors

Team GarlicBread

- Joël Von Der Weid
- Jérôme Chételat
- Anas Guetari
- Maxime Clercq
